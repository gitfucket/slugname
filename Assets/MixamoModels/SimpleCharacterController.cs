﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCharacterController : MonoBehaviour {

    protected Animator anim;
    protected Rigidbody body;

    public Camera main_camera; // need a reference to this to get forward and right

    public float turn_speed = 0.5f;
    public float animation_delta = 0.1f;

    // Stuff for non root motion
    public bool use_root_motion = true;
    public float walk_speed = 0.02f;
    public float run_speed = 0.035f;
    public float acceleration = 0.01f;
    public float deceleration = 0.6f;
    float currentspeed = 0;

  
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        
        if (use_root_motion)
        {
            MoveWithRoot();
        } else
        {
            MoveWithoutRoot();
        }

        if (Input.GetKeyDown("k"))
        {
            use_root_motion = !use_root_motion;
        }
    }

    void MoveWithRoot() {
        float forward = Input.GetAxis("Vertical");
        float right = Input.GetAxis("Horizontal");
        Vector2 input = new Vector2(forward, right);

        float desired_speed = input.magnitude;
        float current_speed = anim.GetFloat("Forward");

        if (desired_speed > 0.01)
        {       
            float new_speed = Mathf.Lerp(current_speed, desired_speed, acceleration);
            anim.SetFloat("Forward", new_speed);
            ApplyRotation(forward, right);
        }
        else
        {
            float new_speed = Mathf.Lerp(current_speed, desired_speed, deceleration);
            anim.SetFloat("Forward", new_speed);
        }

       
    }

    void MoveWithoutRoot()
    {

        // Grab the inputs
        float forward = Input.GetAxis("Vertical");
        float right = Input.GetAxis("Horizontal");
        Vector2 input = new Vector2(forward, right);

        float desired_speed;

        // Set speed - Animtaion is gonna have sharp edges, no blending
        // So we're gonna hack this shid
        float animspeed = anim.GetFloat("Forward");
        if (input.magnitude > 0.6)
        {

            animspeed = Mathf.Lerp(animspeed, 1, animation_delta);
            anim.SetFloat("Forward", animspeed);
            desired_speed = run_speed;

        }
        else if (input.magnitude > 0.1f)
        {
            animspeed = Mathf.Lerp(animspeed, 0.11f, animation_delta);
            anim.SetFloat("Forward", animspeed);
            desired_speed = walk_speed;
        }
        else
        {
            animspeed = Mathf.Lerp(animspeed, 0, animation_delta);
            anim.SetFloat("Forward", animspeed);
            desired_speed = 0;
            //currentspeed /= 2; // hack to quickly reduce speed
        }

        currentspeed = Mathf.Lerp(currentspeed, desired_speed, acceleration);
        Vector3 new_position = transform.position + transform.forward * currentspeed;
        body.MovePosition(new_position);

        ApplyRotation(forward, right);

    }

    void ApplyRotation(float forward, float right)
    {
        // Apply Rotation poorly
        Vector3 relative_right = main_camera.transform.right * right;

        Vector3 relative_forward = main_camera.transform.forward * forward;
        relative_forward.y = 0;

        Vector3 desired_direction = (relative_forward + relative_right).normalized;
        
        Vector3 new_direction = Vector3.RotateTowards(transform.forward, desired_direction, turn_speed, 1);
        transform.LookAt(transform.position + new_direction);
    }
}
