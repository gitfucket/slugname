﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour {

    public Transform player;
    public float height;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (player != null)
        {
            transform.position = new Vector3(transform.position.x, player.transform.position.y + height, transform.position.z);
            transform.LookAt(player.transform.position + Vector3.up * 1.5f);
        }
	}
}
