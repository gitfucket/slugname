﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Controller
{
    public class HandleClimbing : MonoBehaviour
    {

        ClimbPointGrabber cpGrabber;
        StateManager states;
        InputHandler ih;
        public Rigidbody rb;
        

        ClimbPoint currentPoint;
        ClimbPoint targetPoint;
        ClimbingState currentState;

        Dictionary<SimpleDirection, ClimbPointConnection> neighbouringPoints;

        public float lerpSpeed = 0.2f;
        public float arrivalThreshold = 0.02f;

        enum ClimbingState
        {
            onPoint,
            betweenPoints,
            inTransit
        }

        // Use this for initialization
        void Start()
        {
            if (cpGrabber == null)
            {
                Debug.Log("No cpGrabber detected, looking for a ClimbPointGrabber in children now!");
                cpGrabber = GetComponentInChildren<ClimbPointGrabber>();
            }
            states = GetComponent<StateManager>();
            ih = GetComponent<InputHandler>();
            rb = GetComponent<Rigidbody>();
            neighbouringPoints = new Dictionary<SimpleDirection, ClimbPointConnection>();
        }


        public void TryToClimb(out bool climbing)
        {
            targetPoint = cpGrabber.GetClimbPoint();

            if (targetPoint)
            {
                climbing = true;
                rb.velocity = Vector3.zero;

                currentState = ClimbingState.inTransit;
                

            }
            else
            {
                climbing = false;
            }
        }

        void Update()
        {
            foreach(SimpleDirection dir in neighbouringPoints.Keys)
            {
                //Debug.Log(item.Key + " " + item.Value);
                Color c = Color.white;
                switch (dir)
                {
                    case SimpleDirection.up:
                        c = Color.green;
                        break;
                    case SimpleDirection.down:
                        c = Color.green;
                        break;
                    case SimpleDirection.left:
                        c = Color.yellow;
                        break;
                    case SimpleDirection.right:
                        c = Color.yellow;
                        break;
                    case SimpleDirection.up_left:
                        c = Color.blue;
                        break;
                    case SimpleDirection.up_right:
                        c = Color.blue;
                        break;
                    case SimpleDirection.down_left:
                        c = Color.red;
                        break;
                    case SimpleDirection.down_right:
                        c = Color.red;
                        break;
                }
                //Debug.DrawLine(neighbouringPoints[dir].position, currentPoint.transform.position, c);
            }
        }

        public void Tick()
        {
            if (!currentPoint && !targetPoint)
            {
                Debug.Log("Nothing to climb on!");
                states.climbing = false;
                return;
            }

            // when we arrive at a point, we should get a set of available targets
            // one for each direction




            
            switch (currentState) {
                case ClimbingState.inTransit:
                    // In transit
                    // lerp towards target point until you arrive
                    // don't accept input
                    transform.position = Vector3.LerpUnclamped(transform.position, targetPoint.bodyPosition, lerpSpeed);
                    transform.rotation = Quaternion.LerpUnclamped(transform.rotation, targetPoint.transform.rotation, lerpSpeed);

                    if(Vector3.Distance(transform.position, targetPoint.bodyPosition) < arrivalThreshold)
                    {
                        Debug.Log("Arrived");
                        currentState = ClimbingState.onPoint;
                        currentPoint = targetPoint;
                        targetPoint = null;
                        SetNeighbours(); // should be done on arrival
                        states.animJumpIndex = 0;
                    }


                    break;

                case ClimbingState.onPoint:
                    // on point
                    // stick to the point (in case it moves)
                    transform.position = currentPoint.bodyPosition;
                    transform.rotation = currentPoint.transform.rotation;

                    // Find where the target ClimbPoint is based on input
                    Vector3 inputDir = new Vector3(states.horizontal, states.vertical, 0);

                    SimpleDirection dir = SimplifyDirection(inputDir);

                    //Debug.Log("current direction: " + dir);

                    ClimbPointConnection next;
                    if (neighbouringPoints.TryGetValue(dir, out next))
                    {
                        Debug.DrawLine(currentPoint.transform.position, next.position, Color.red);
                        Debug.Log("Moving!");
                        targetPoint = next.point;
                        currentState = ClimbingState.inTransit;
                        states.animJumpIndex = 1;
                        neighbouringPoints.Clear(); // do on move
                    }


                    break;
            }
        }

        void MoveToConnectedClimbPoint()
        {

        }
        
        void SetNeighbours(){
            // currentPoint != null or it won't work

            neighbouringPoints.Clear();

            foreach (ClimbPoint p in cpGrabber.GetAllAvailableClimbPoints())
            {
                if (p == currentPoint)
                {
                    Debug.Log("skipping current point");
                    continue;
                }
                // get direction of p
                // switch(direction)
                //      swap if it's closer than current direction in neighbouringPoints
                Vector3 directionToPoint = (p.transform.position - currentPoint.transform.position).normalized;
                SimpleDirection d = SimplifyDirection(directionToPoint);


                if (d == SimpleDirection.neutral)
                {
                    Debug.Log("direction to Point: " + directionToPoint);
                    Debug.DrawLine(p.transform.position, currentPoint.transform.position, Color.cyan, 5);
                    Debug.Log("atan2()" + Mathf.Atan2(directionToPoint.x, directionToPoint.y) * Mathf.Rad2Deg);
                    continue;
                    Debug.Log("distance to this point: " + Vector3.Distance(p.transform.position, currentPoint.transform.position));
                    
                    
                }


                ClimbPointConnection currentClosest;
                if (neighbouringPoints.TryGetValue(d, out currentClosest))
                {
                    float distanceToBeat = Vector3.Distance(currentClosest.position, currentPoint.transform.position);
                    if (Vector3.Distance(p.transform.position, currentPoint.transform.position) < distanceToBeat)
                    {
                        neighbouringPoints.Remove(d);
                        neighbouringPoints.Add(d, new ClimbPointConnection(p));
                        //Debug.Log("added new point in direction: " + d);
                    }
                    else
                    {
                        //Debug.Log("old d was better for " + d);
                    }
                }
                else // musta been null so...
                {
                    //...just set it
                    neighbouringPoints.Add(d, new ClimbPointConnection(p));
                    //Debug.Log("added new point in direction: " + d);
                }
            }
            //Debug.Log("neighbour count " + neighbouringPoints.Values.Count);
        }



        SimpleDirection SimplifyDirection(Vector3 direction)
        {
            SimpleDirection retVal = SimpleDirection.neutral;

            if (direction.magnitude < 0.4f)
                return retVal;

            float targetAngle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
            targetAngle = ClampAngle360(targetAngle);
            //Debug.Log("atan2(" + direction.x + ", " + direction.y + ") = " + targetAngle);

            if (targetAngle < 22.5f || targetAngle > 360 - 22.5f)
            {
                retVal = SimpleDirection.up;
            }
            else if (targetAngle < 45 + 22.5f && targetAngle > 45 - 22.5f)
            {
                retVal = SimpleDirection.up_right;
            }
            else if (targetAngle < 90 + 22.5f && targetAngle > 90 - 22.5f)
            {
                retVal = SimpleDirection.right;
            }
            else if (targetAngle < 135 + 22.5f && targetAngle > 135 - 22.5f)
            {
                retVal = SimpleDirection.down_right;
            }
            else if (targetAngle < 180 + 22.5f && targetAngle > 180 - 22.5f)
            {
                retVal = SimpleDirection.down;
            }
            else if (targetAngle < 225 + 22.5f && targetAngle > 225 - 22.5f)
            {
                retVal = SimpleDirection.down_left;
            }
            else if (targetAngle < 270 + 22.5f && targetAngle > 270 - 22.5f)
            {
                retVal = SimpleDirection.left;
            }
            else if (targetAngle < 315 + 22.5f && targetAngle > 315 - 22.5f)
            {
                retVal = SimpleDirection.up_left;
            }

            /*
            if (targetAngle < 22.5f && targetAngle > -22.5f)
            {
                retVal = SimpleDirection.up;
            }
            else if (targetAngle < 45 + 22.5f && targetAngle > 45 - 22.5f)
            {
                retVal = SimpleDirection.up_right;
            }
            else if (targetAngle < 90 + 22.5f && targetAngle > 90 - 22.5f)
            {
                retVal = SimpleDirection.right;
            }
            else if (targetAngle < 135 + 22.5f && targetAngle > 135 - 22.5f)
            {
                retVal = SimpleDirection.down_right;
            }
            else if (targetAngle < 180 + 22.5f && targetAngle > 180 - 22.5f)
            {
                retVal = SimpleDirection.down;
            }
            else if (targetAngle < -135 + 22.5f && targetAngle > -135 - 22.5f)
            {
                retVal = SimpleDirection.down_left;
            }
            else if (targetAngle < -90 + 22.5f && targetAngle > -90 - 22.5f)
            {
                retVal = SimpleDirection.left;
            }
            else if (targetAngle < -45 + 22.5f && targetAngle > -45 - 22.5f)
            {
                retVal = SimpleDirection.up_left;
            }
            */

            return retVal;
        }

        float ClampAngle360(float angle)
        {
            while (angle < 0)
            {
                angle += 360;
            }
            while (angle > 360)
            {
                angle -= 360;
            }

            return angle;
        }

    }


    class ClimbPointConnection
    {
        public ClimbPointConnectionType connectionType;
        public ClimbPoint point;
        public Vector3 position
        {
            get { return point.transform.position; }
        }

        public ClimbPointConnection(ClimbPoint p, ClimbPointConnectionType type = ClimbPointConnectionType.direct)
        {
            point = p;
            connectionType = type;
        }

    }

    enum ClimbPointConnectionType
    {
        direct,
        twoStep
    }

    enum SimpleDirection
    {
        up,
        down,
        left,
        right,
        up_left,
        up_right,
        down_left,
        down_right,
        neutral
    }

}