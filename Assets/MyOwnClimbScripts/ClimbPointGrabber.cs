﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbPointGrabber : MonoBehaviour {

    SphereCollider grabCollider;
    public float grabRange = 0.5f;
    public float grabSliceHeight = 0.1f;

    private List<ClimbPoint> climbPointsInRange;

    // Use this for initialization
    void Start() {
        grabCollider = GetComponent<SphereCollider>();
        climbPointsInRange = new List<ClimbPoint>();
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other) {
        if (other.GetComponent<ClimbPoint>())
        {
            climbPointsInRange.Add(other.GetComponent<ClimbPoint>());
            //Debug.Log("My grabber collided with a climb point!");
        }
    }

    void OnTriggerExit(Collider other)
    {
        climbPointsInRange.Remove(other.GetComponent<ClimbPoint>());
        //Debug.Log("Removed a point");
    }

    public ClimbPoint GetClimbPoint()
    {
        ClimbPoint retVal = null;
        foreach (ClimbPoint p in climbPointsInRange)
        {
            if (Vector3.Distance(p.transform.position, transform.position) < grabRange)
            {
                if (Mathf.Abs(p.transform.position.y - transform.position.y) < grabSliceHeight)
                {
                    retVal = p;
                    break;
                }

            }
        }
        return retVal;
    }

    public List<ClimbPoint> GetAllAvailableClimbPoints()
    {
        return climbPointsInRange;
    }

}
