﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipController : MonoBehaviour {

    public Thruster[] thrusters;
    private Vector3 thrustPoint;

    public float thrustStrength = 100f;

    public float antiDriftStrength = 600.0f;
    public float driftCorrectionfactor = 10.0f;

    public float antiSpinStrength = 1f;

    public Rigidbody body;

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
        thrusters = GetComponentsInChildren<Thruster>();
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        float lift = Input.GetAxis("RS_Y");
        float yaw = Input.GetAxis("RS_X");
        float pitch = Input.GetAxis("LS_Y");
        float roll = Input.GetAxis("LS_X");

        float forward = Input.GetAxis("RightTrigger");
        float reverse = Input.GetAxis("LeftTrigger");
        float thrust = (reverse - forward)/2;

        bool antiDrift = Input.GetButton("Fire2");
        bool antiSpin = Input.GetButton("Fire1");

        foreach (Thruster thruster in thrusters)
        {
            Vector3 force = thruster.transform.up * thrustStrength * thrust;
            thruster.SetLightIntensity(Mathf.Abs(thrust));
            body.AddForceAtPosition(force, thruster.transform.position);
        }

        Vector3 thrustPoint = Vector3.zero;

        body.AddForce(transform.up * -lift);

        body.AddTorque(transform.up * yaw);
        body.AddTorque(transform.forward * -roll);
        body.AddTorque(transform.right * -pitch);


        if (antiDrift)
        {
            Vector3 localVelocity = transform.InverseTransformDirection(body.velocity);

            //Stabalize Up/Down
            float verticalDrift = localVelocity.y;
            float verticalCorrection = Clamp(-verticalDrift * driftCorrectionfactor, antiDriftStrength); 

            //Stabalize Left/Right
            float horizontalDrift = localVelocity.x;
            float horizontalCorrection = Clamp(-horizontalDrift * driftCorrectionfactor, antiDriftStrength);

            Vector3 stabalizingForce = transform.right * horizontalCorrection + transform.up * verticalCorrection;

            body.AddForce(stabalizingForce);

            Debug.Log("velocity: " + body.velocity);
            Debug.Log("localVelocity: " + localVelocity);
            Debug.Log("horizontalDrift: " + horizontalDrift + "    verticalDrift: " + verticalDrift);
            Debug.Log("CorrectionVector" + stabalizingForce);
        }

        if (antiSpin)
        {
            Vector3 angularVelocity = transform.InverseTransformDirection(body.angularVelocity);

            float pitchCorrection = Clamp(-angularVelocity.x * driftCorrectionfactor, antiSpinStrength);
            float yawCorrection = Clamp(-angularVelocity.y * driftCorrectionfactor, antiSpinStrength);
            float rollCorrection = Clamp(-angularVelocity.z * driftCorrectionfactor, antiSpinStrength);

            body.AddTorque(transform.up * yawCorrection);
            body.AddTorque(transform.forward * rollCorrection);
            body.AddTorque(transform.right * pitchCorrection);

            Debug.Log("angular velocity: " + body.angularVelocity);
            Debug.Log("local angular velocity: " + angularVelocity);
            //float rollCorrection = Clamp()
        }

        //Debug.Log(body.angularVelocity);
        //PrintControls();
        //Debug.Log("Overall Thrust: " + thrust);

    }

    void PrintControls()
    {
        Debug.Log("LS_Y: " + Input.GetAxis("LS_Y") + "    RS_Y: " + Input.GetAxis("RS_Y"));
        Debug.Log("LS_X: " + Input.GetAxis("LS_X") + "    RS_X: " + Input.GetAxis("RS_X"));
        Debug.Log("LeftTrigger: " + Input.GetAxis("LeftTrigger") + "    RightTrigger: " + Input.GetAxis("RightTrigger"));
        Debug.Log("AnitDrift: " + Input.GetButton("Fire2"));
        Debug.Log("AntiSpin: " + Input.GetButton("Fire1"));
    }


    float Clamp(float x, float limit)
    {
        if (Mathf.Abs(x) > Mathf.Abs(limit))
            return Mathf.Sign(x) * limit;
        else
            return x;
    }

}
