﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereInfluencer : MonoBehaviour {

    Rigidbody body;

    public float force = 20f;

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        float hori = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 input = new Vector3(hori, 0, vert);

        body.AddForce(input);

	}
}
