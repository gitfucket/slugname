﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour {

    Rigidbody body;

    public float temp_strength = 20f;

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

        ApplyTorque(Input.GetAxis("Vertical") * temp_strength);

	}

    void ApplyTorque(float force)
    {
        body.AddTorque(transform.up * force);
    }

}
