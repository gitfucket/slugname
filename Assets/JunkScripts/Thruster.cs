﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : MonoBehaviour {

    public float fuel = 200.0f;
    public float force = 100.0f;

    public Light light;

    // Use this for initialization
	void Start () {
        light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetLightIntensity(float intensity)
    {
        light.intensity = intensity;
    }
}
