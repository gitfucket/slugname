﻿using UnityEngine;
using System.Collections;

namespace Controller
{
    public class HandleAnim : MonoBehaviour
    {

        StateManager states;
        public Animator anim;

        public void Init(StateManager st)
        {
            states = st;
            anim = GetComponent<Animator>();
            
            Animator[] childAnims = GetComponentsInChildren<Animator>();

            for (int i = 0; i < childAnims.Length; i++)
            {
                if(childAnims[i] != anim)
                {
                    anim.avatar = childAnims[i].avatar;
                    Destroy(childAnims[i]);
                    break;
                }
            }
        }

        public void Tick()
        {
            anim.SetFloat("Movement", states.animSpeed);
            anim.SetBool("Climbing", states.climbing);
            anim.SetBool("MidAir", !states.onGround);
            anim.SetInteger("JumpIndex", states.animJumpIndex);
        }
    }
}
