﻿using UnityEngine;
using System.Collections;

namespace Controller
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(HandleAnim))]
    public class StateManager : MonoBehaviour
    {
        // input variables
        public float horizontal;
        public float vertical;
        public bool jumpFrame;
        public bool climbButton;
        public bool dismountButton;

        public float animSpeed;
        public int animJumpIndex;

        public bool dummy;
        public bool onGround = true;
        public bool climbing = false;

        [HideInInspector]
        public HandleAnim hAnim;
        [HideInInspector]
        public HandleMovement hMovement;
        [HideInInspector]
        public HandleClimbing hClimbing;

        [HideInInspector]
        CapsuleCollider movementCollider;

        void Start()
        {
            hAnim = GetComponent<HandleAnim>();
            hMovement = GetComponent<HandleMovement>();
            hClimbing = GetComponent<HandleClimbing>();

            movementCollider = GetComponent<CapsuleCollider>();

            hAnim.Init(this);
            hMovement.Init();
        }

        void Update()
        {
            if (!dummy)
            {
                hAnim.Tick();

                CheckGround();
                CheckClimbing();

                if (climbing)
                    hClimbing.Tick();
                else
                    hMovement.Tick();
              
            }
        }

        public void EnableController()
        {
            dummy = false;
            hMovement.rb.isKinematic = false;
            GetComponent<Collider>().isTrigger = false;
        }

        public void DisableController()
        {
            dummy = true;
            hMovement.rb.isKinematic = true;
            GetComponent<Collider>().isTrigger = true;
        }

        void CheckGround()
        {
            onGround = !climbing && OnGroundTest();     
        }

        bool OnGroundTest()
        {
            bool retVal = false;

            Vector3 origin = transform.position + Vector3.up / 18;
            Vector3 direction = -Vector3.up;
            float distance =  0.2f;
            LayerMask lm = ~(1 << gameObject.layer);
            RaycastHit hit;

            Debug.DrawLine(origin, origin + direction * distance, Color.red);
            if(Physics.Raycast(origin, direction, out hit, distance, lm))
            {
                if (hit.transform.gameObject.layer == gameObject.layer)
                    Debug.Log("OnGround hit an object with the same layer as the controller!!");

                retVal = true;
            }

            return retVal;
        }


        void CheckClimbing()
        {
            if (climbing)
            {
                if (dismountButton)
                {
                    climbing = false;
                }
                if (jumpFrame)
                {
                    climbing = false;
                    hMovement.Jump();
                }
            }
            else
            {
                if (climbButton || true)
                {
                    // the 'out' keyword means the following variable will be assigned by the function!
                    // climbing will be set to true if TryyToClimb succeeds!
                    hClimbing.TryToClimb(out climbing);
                }
            }

            // disable movement collider while climbing
            movementCollider.enabled = !climbing;

        }
    }
}
