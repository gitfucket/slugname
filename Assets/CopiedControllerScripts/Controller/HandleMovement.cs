﻿using UnityEngine;
using System.Collections;

namespace Controller
{
    public class HandleMovement : MonoBehaviour
    {

        public Rigidbody rb;
        StateManager states;
        InputHandler ih;

        public float acceleration = 0.5f;
        public float runSpeed = 4.25928f; // calculated by the animation /// 4.595691 for blacks
        public float walkSpeed = 1.50048f; // this too // 1.609258 for blacki
        public float rotateSpeed = 8;

        public float airControlMult = 0.2f;
        public float jumpSpeedMult = 1.1f;
        public float jumpStrength = 8f;
        float TERMINAL_VELOCITY = 53f; // 53 m/s

        float desiredSpeed = 0;
        Vector3 desiredVelocity = Vector3.zero;



        Vector3 storeDirection;

        public void Init()
        {
            states = GetComponent<StateManager>();
            ih = GetComponent<InputHandler>();

            rb = GetComponent<Rigidbody>();
            rb.drag = 0;
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }

        public void Tick()
        {
            // get direction
            Vector3 v = ih.camHolder.forward;
            v.y = 0;
            v.Normalize();
            v *= states.vertical;

            Vector3 h = ih.camHolder.right;
            h.y = 0;
            h.Normalize();
            h *= states.horizontal;

            // get speed
            desiredSpeed = Mathf.Sqrt(states.horizontal * states.horizontal + states.vertical * states.vertical);

            // fiddle with stopping and starting to prevent awkward shuffle
            if (desiredSpeed > 0.15f) // only move with sufficient input
            {
                desiredVelocity = (h + v).normalized * MapToRange(desiredSpeed, walkSpeed, runSpeed);
            }
            else // gradually slow down to stop without enough input
            {
                desiredVelocity = Vector3.Lerp(rb.velocity, Vector3.zero, 0.5f);
            }
            desiredVelocity.y = rb.velocity.y; // handle vertical velocity target seperattetley 

            //Debug.Log("Velocity " + rb.velocity +       "    mag: " + rb.velocity.magnitude);
            //Debug.Log("desired v " + desiredVelocity +  "    mag: " + desiredVelocity.magnitude);
            //Debug.Log("lerped v " + Vector3.Lerp(rb.velocity, desiredVelocity, acceleration) + "   mag: " + Vector3.Lerp(rb.velocity, desiredVelocity, acceleration).magnitude);
            
            if (states.onGround )
            {
                rb.velocity = Vector3.Lerp(rb.velocity, desiredVelocity, acceleration);

                if (states.jumpFrame)
                {
                    Jump();
                }

            }
            else // in air...
            {
                rb.velocity = Vector3.Lerp(rb.velocity, desiredVelocity, acceleration * airControlMult);

                Vector3 gravityVector = Vector3.down * 9.81f * Time.deltaTime;
                if ((rb.velocity + gravityVector).magnitude > TERMINAL_VELOCITY)
                {
                    rb.velocity = rb.velocity.normalized * TERMINAL_VELOCITY;
                    //Debug.Log("EEEEEEEEEEEEEEEEEEEEEEEEEE");
                }
                else
                {
                    rb.velocity += gravityVector;
                }
                
            }

            states.animSpeed = (rb.velocity.magnitude > 0.9f) ? rb.velocity.magnitude : 0;

            //Debug.Log("nu velo " + rb.velocity + "    mag: " + rb.velocity.magnitude);
            //Debug.Log("speed: " + rb.velocity.magnitude);
            //Debug.Log("animSpeed: " + states.animSpeed);

            // Rotaten (shuold make this rotate to face velocity)
            if (Mathf.Abs(states.vertical) > 0 || Mathf.Abs(states.horizontal) > 0)
            {
                storeDirection = (v + h).normalized;
                storeDirection += transform.position;
                Vector3 targetDir = (storeDirection - transform.position).normalized;
                targetDir.y = 0;

                if (targetDir == Vector3.zero)
                    targetDir = transform.forward;

                Quaternion targetRot = Quaternion.LookRotation(targetDir);
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, rotateSpeed * Time.deltaTime);
            }
        }


        public void Jump()
        {
            rb.velocity *= jumpSpeedMult; // adds kick
            rb.velocity += Vector3.up * jumpStrength; // adds height
        }


        // This is used to map 0,1 ranges from input to arbitrary range
        // ie [0 -> lo]  |  [1 -> hi]  |  [x -> (some value between lo and hi)]
        // The walking animation blends based on actual speed, normally between around 1.5 and 4.2
        float MapToRange(float x, float lo, float hi)
        {
            // [0,1] -> [lo,hi]
            float diff = hi - lo;
            float percent = Mathf.Min(1, Mathf.Max(0, x));
            return lo + diff * percent;
        }

        float MapTo01(float x, float lo, float hi)
        {
            // [lo,hi] -> [0,1]
            float range = hi - lo;
            float ret = x - lo;
            ret *= 1 / range;

            return ret;
        }

    }
}
